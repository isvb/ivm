﻿using Ivm.Domain;

namespace Ivm.Storage
{
    public interface IStorage
    {
        int GetNextVersion();
        
        void Save(string param, string value, int version);
        
        (int Version, string Value) Get(string param);
        
        void SaveResult(int version, string value);
        
        string GetResult(int version);

        Graph GetDependencyGraph();
    }
}