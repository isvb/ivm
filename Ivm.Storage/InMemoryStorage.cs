﻿using System;
using System.Collections.Generic;
using Ivm.Domain;
using Microsoft.Extensions.Logging;

namespace Ivm.Storage
{
    // Not thread safe - I assume that this is out of scope
    public class InMemoryStorage : IStorage
    {
        private int _version = 0;
        private Dictionary<string, (int Ver, string value)> _storage = new();
        private Dictionary<int, string> _results = new();
        
        private readonly ILogger<InMemoryStorage> _logger;

        public InMemoryStorage(ILogger<InMemoryStorage> logger)
        {
            _logger = logger;
        }

        public int GetNextVersion() => ++_version;

        public void Save(string param, string value, int version)
        {
            if (!_storage.TryGetValue(param, out var existingRecord))
            {
                _storage[param] = (version, value);
                return;
            }

            _storage[param] = (Math.Max(version, existingRecord.Ver), value);
        }

        public void SaveResult(int version, string value)
        {
            _results[version] = value;
            _logger.LogDebug($"Result: {version} - {value}");
        }

        public string GetResult(int version) => _results[version];

        public (int Version, string Value) Get(string param)
        {
            if (!_storage.TryGetValue(param, out var record))
                return (0, "null");

            return record;
        }

        public Graph GetDependencyGraph()
        {
            var result = new Graph();
            result.AddVertex(WellKnownParams.A, WellKnownParams.Fab);
            result.AddVertex(WellKnownParams.B, WellKnownParams.Fab);
            result.AddVertex(WellKnownParams.C, WellKnownParams.Fcd);
            result.AddVertex(WellKnownParams.D, WellKnownParams.Fcd);
            result.AddVertex(WellKnownParams.E, WellKnownParams.Fe);
            result.AddVertex(WellKnownParams.X, WellKnownParams.Fx);
            result.AddVertex(WellKnownParams.Y, WellKnownParams.Fy);
            result.AddVertex(WellKnownParams.Fab, WellKnownParams.Fcd);
            result.AddVertex(WellKnownParams.Fy, WellKnownParams.Fcd);
            result.AddVertex(WellKnownParams.Fx, WellKnownParams.Fe);
            result.AddVertex(WellKnownParams.Fcd, WellKnownParams.Fe);
            return result;
        }
    }
}