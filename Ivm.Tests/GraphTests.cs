﻿using FluentAssertions;
using Ivm.Domain;
using Xunit;

namespace Ivm.Tests
{
    public class GraphTests
    {
        [Fact]
        public void GetNodesToVisitShouldReturnCorrectCollection()
        {
            var graph = CreateGraph();

            var nodesToVisit = graph.GetNodesToVisit(new[] { "A", "E", "X" });
            nodesToVisit.Should().BeEquivalentTo("Fab", "Fcd", "Fx", "Fe");
        } 

        private Graph CreateGraph()
        {
            var result = new Graph();
            result.AddVertex(WellKnownParams.A, WellKnownParams.Fab);
            result.AddVertex(WellKnownParams.B, WellKnownParams.Fab);
            result.AddVertex(WellKnownParams.C, WellKnownParams.Fcd);
            result.AddVertex(WellKnownParams.D, WellKnownParams.Fcd);
            result.AddVertex(WellKnownParams.E, WellKnownParams.Fe);
            result.AddVertex(WellKnownParams.X, WellKnownParams.Fx);
            result.AddVertex(WellKnownParams.Y, WellKnownParams.Fy);
            result.AddVertex(WellKnownParams.Fab, WellKnownParams.Fcd);
            result.AddVertex(WellKnownParams.Fy, WellKnownParams.Fcd);
            result.AddVertex(WellKnownParams.Fx, WellKnownParams.Fe);
            result.AddVertex(WellKnownParams.Fcd, WellKnownParams.Fe);
            return result;
        }
    }
}