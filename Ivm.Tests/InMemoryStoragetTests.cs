using FluentAssertions;
using Ivm.Storage;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace Ivm.Tests
{
    public class InMemoryStorageTests
    {
        [Fact]
        public void GetNextVersionShouldReturnNextVersion()
        {
            var inMemoryStorage = new InMemoryStorage(Mock.Of<ILogger<InMemoryStorage>>());
            inMemoryStorage.GetNextVersion().Should().Be(1);
            inMemoryStorage.GetNextVersion().Should().Be(2);
            inMemoryStorage.GetNextVersion().Should().Be(3);
        }

        [Fact]
        public void SaveShouldSaveValueAndVersionWhenCalledFirstTime()
        {
            var inMemoryStorage = new InMemoryStorage(Mock.Of<ILogger<InMemoryStorage>>());
            inMemoryStorage.Save("my-param", "my-value", 42);
            inMemoryStorage.Get("my-param").Should().Be((42, "my-value"));
        }
        
        [Fact]
        public void SaveShouldUpdateValueWhenSomeValueAlreadyExist()
        {
            var inMemoryStorage = new InMemoryStorage(Mock.Of<ILogger<InMemoryStorage>>());
            inMemoryStorage.Save("my-param", "my-value-1", 42);
            inMemoryStorage.Save("my-param", "my-value-2", 42);
            inMemoryStorage.Get("my-param").Should().Be((42, "my-value-2"));
        }
        
        [Fact]
        public void SaveShouldUpdateVersionWhenVersionIsGtExisting()
        {
            var inMemoryStorage = new InMemoryStorage(Mock.Of<ILogger<InMemoryStorage>>());
            inMemoryStorage.Save("my-param", "my-value", 1);
            inMemoryStorage.Save("my-param", "my-value", 2);
            inMemoryStorage.Get("my-param").Should().Be((2, "my-value"));
        }
        
        [Fact]
        public void SaveShouldNotUpdateVersionWhenVersionIsLtExisting()
        {
            var inMemoryStorage = new InMemoryStorage(Mock.Of<ILogger<InMemoryStorage>>());
            inMemoryStorage.Save("my-param", "my-value", 2);
            inMemoryStorage.Save("my-param", "my-value", 1);
            inMemoryStorage.Get("my-param").Should().Be((2, "my-value"));
        }
    }
}