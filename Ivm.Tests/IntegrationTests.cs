﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using Ivm.App;
using Ivm.App.Requests;
using Ivm.Domain;
using Ivm.Storage;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace Ivm.Tests
{
    public class IntegrationTests
    {
        private IServiceProvider _serviceProvider;
        
        public IntegrationTests()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddTransient<UpdateParametersCommand>();
            serviceCollection.AddSingleton<IStorage, InMemoryStorage>();
            serviceCollection.AddSingleton<IRequestFactory, RequestFactory>();
            serviceCollection.AddSingleton<IFunctions, Functions>();
            serviceCollection.AddMediatR(typeof(Fe));
            serviceCollection.AddLogging();
            _serviceProvider = serviceCollection.BuildServiceProvider();
        }
        
        [Fact]
        public async Task UpdateAShouldReturnCorrectResult()
        {
            var command = GetCommand();
            var storage = GetStorage();
            await command.Execute(new[] { (WellKnownParams.A, "a1") });
            var result = storage.GetResult(1);
            var expectedResult = "Fe(null,Fcd(null,null,Fab(a1,null),null),null)";
            result.Should().Be(expectedResult);
        }
        
        [Fact]
        public async Task UpdateATwiceShouldReturnCorrectResult()
        {
            var command = GetCommand();
            var storage = GetStorage();
            await command.Execute(new[] { (WellKnownParams.A, "a1") });
            await command.Execute(new[] { (WellKnownParams.A, "a2") });
            var result = storage.GetResult(2);
            var expectedResult = "Fe(null,Fcd(null,null,Fab(a2,null),null),null)";
            result.Should().Be(expectedResult);
        }
        
        [Fact]
        public async Task UpdateABShouldReturnCorrectResult()
        {
            var command = GetCommand();
            var storage = GetStorage();
            await command.Execute(new[]
            {
                (WellKnownParams.A, "a1"),
                (WellKnownParams.B, "b1")
            });
            var result = storage.GetResult(1);
            var expectedResult = "Fe(null,Fcd(null,null,Fab(a1,b1),null),null)";
            result.Should().Be(expectedResult);
        }
        
        [Fact]
        public async Task UpdateAXShouldReturnCorrectResult()
        {
            var command = GetCommand();
            var storage = GetStorage();
            await command.Execute(new[]
            {
                (WellKnownParams.A, "a1"),
                (WellKnownParams.X, "x1")
            });
            var result = storage.GetResult(1);
            var expectedResult = "Fe(null,Fcd(null,null,Fab(a1,null),null),Fx(x1))";
            result.Should().Be(expectedResult);
        }
        
        [Fact]
        public async Task UpdateACXShouldReturnCorrectResult()
        {
            var command = GetCommand();
            var storage = GetStorage();
            await command.Execute(new[]
            {
                (WellKnownParams.A, "a1"),
                (WellKnownParams.C, "c1"),
                (WellKnownParams.X, "x1")
            });
            var result = storage.GetResult(1);
            var expectedResult = "Fe(null,Fcd(c1,null,Fab(a1,null),null),Fx(x1))";
            result.Should().Be(expectedResult);
        }
        
        [Fact]
        public async Task UpdateAllShouldReturnCorrectResult()
        {
            var command = GetCommand();
            var storage = GetStorage();
            await command.Execute(new[]
            {
                (WellKnownParams.A, "a1"),
                (WellKnownParams.B, "b1"),
                (WellKnownParams.C, "c1"),
                (WellKnownParams.D, "d1"),
                (WellKnownParams.E, "e1"),
                (WellKnownParams.X, "x1"),
                (WellKnownParams.Y, "y1")
            });
            var result = storage.GetResult(1);
            var expectedResult = "Fe(e1,Fcd(c1,d1,Fab(a1,b1),Fy(y1)),Fx(x1))";
            result.Should().Be(expectedResult);
        }
        
        [Fact]
        public async Task UpdateAllThenUpdateAXShouldReturnCorrectResult()
        {
            var command = GetCommand();
            var storage = GetStorage();
            await command.Execute(new[]
            {
                (WellKnownParams.A, "a1"),
                (WellKnownParams.B, "b1"),
                (WellKnownParams.C, "c1"),
                (WellKnownParams.D, "d1"),
                (WellKnownParams.E, "e1"),
                (WellKnownParams.X, "x1"),
                (WellKnownParams.Y, "y1")
            });
            await command.Execute(new[]
            {
                (WellKnownParams.A, "a2"),
                (WellKnownParams.X, "x2")
            });
            var result = storage.GetResult(2);
            var expectedResult = "Fe(e1,Fcd(c1,d1,Fab(a2,b1),Fy(y1)),Fx(x2))";
            result.Should().Be(expectedResult);
        }

        private UpdateParametersCommand GetCommand() 
            => _serviceProvider.GetRequiredService<UpdateParametersCommand>();
        
        private IStorage GetStorage() 
            => _serviceProvider.GetRequiredService<IStorage>();
    }
}