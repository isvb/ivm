﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ivm.App;
using Microsoft.AspNetCore.Mvc;

namespace Ivm.WebHost.Controllers
{
    [ApiController]
    public class Controller : ControllerBase
    {
        private readonly UpdateParametersCommand _updateParametersCommand;

        public Controller(UpdateParametersCommand updateParametersCommand)
        {
            _updateParametersCommand = updateParametersCommand;
        }
        
        [HttpPost]
        [Route("parameters")]
        [ProducesResponseType(204)]
        public async Task<IActionResult> Parameters(IDictionary<string, string> parameters)
        {
            await _updateParametersCommand.Execute(parameters.Select(x => (x.Key, x.Value)).ToArray());
            return new NoContentResult();
        }
    }
}