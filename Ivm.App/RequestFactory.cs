﻿using System;
using System.Collections.Generic;
using Ivm.App.Requests;
using MediatR;

namespace Ivm.App
{
    public interface IRequestFactory
    {
        IRequest Create(string param, int version, HashSet<string> nodesToVisit);
    }

    public class RequestFactory : IRequestFactory
    {
        public IRequest Create(string func, int version, HashSet<string> nodesToVisit) => func switch
        {
            "Fab" => new Fab(version, nodesToVisit),
            "Fcd" => new Fcd(version, nodesToVisit),
            "Fe" => new Fe(version, nodesToVisit),
            "Fx" => new Fx(version, nodesToVisit),
            "Fy" => new Fy(version, nodesToVisit),
            _ => throw new ArgumentOutOfRangeException()
        };
    }
}