﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ivm.Domain;
using Ivm.Storage;
using MediatR;

namespace Ivm.App
{
    public sealed class UpdateParametersCommand
    {
        private readonly IStorage _storage;
        private readonly IRequestFactory _requestFactory;
        private readonly IMediator _mediator;

        public UpdateParametersCommand(IStorage storage, IRequestFactory requestFactory, IMediator mediator)
        {
            _storage = storage;
            _requestFactory = requestFactory;
            _mediator = mediator;
        }
        
        public async Task Execute((string Param, string Value)[] parameters)
        {
            var version = _storage.GetNextVersion();
            foreach ((string param, string value) in parameters)
            {
                _storage.Save(param, value, version);
            }

            var dependencyGraph = _storage.GetDependencyGraph();
            var nodesToVisit = dependencyGraph.GetNodesToVisit(parameters.Select(x => x.Param));

            var adjacentFunctions = GetListOfAdjacentFunctions(parameters.Select(x => x.Param), dependencyGraph);
            foreach (var func in adjacentFunctions)
            {
                var request = _requestFactory.Create(func, version, nodesToVisit);
                await _mediator.Send(request);
            }
        }

        private IEnumerable<string> GetListOfAdjacentFunctions(IEnumerable<string> parameters, Graph graph)
        {
            var adjacentNodes = new HashSet<string>();
            foreach (var param in parameters)
            {
                var adjacentNode = graph.GetAdjacentNode(param);
                adjacentNodes.Add(adjacentNode);
            }

            return adjacentNodes;
        }
    }
}
