﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Ivm.Domain;
using Ivm.Storage;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Ivm.App.Requests
{
    public class Fe : IRequest
    {
        public Fe(int version, HashSet<string> parameters)
        {
            Version = version;
            Parameters = parameters;
        }
        
        public int Version { get; set; }
        
        /// <summary>
        /// Set of parameters that must have version >= <code>Version</code>
        /// </summary>
        public HashSet<string> Parameters { get; set; }
    }
    
    public class FeHandler : IRequestHandler<Fe>
    {
        private readonly IStorage _storage;
        private readonly IFunctions _functions;
        private readonly ILogger<Fe> _logger;

        public FeHandler(IStorage storage, IFunctions functions, ILogger<Fe> logger)
        {
            _storage = storage;
            _functions = functions;
            _logger = logger;
        }
        
        public Task<Unit> Handle(Fe request, CancellationToken cancellationToken)
        {
            _logger.LogTrace($"Executing {nameof(Fe)}: {request?.Version}");
            
            var (fcdVer, Fcd) = _storage.Get(WellKnownParams.Fcd);
            if (fcdVer < request.Version && request.Parameters.Contains(WellKnownParams.Fcd))
                return Unit.Task;
            
            var (fxVer, Fx) = _storage.Get(WellKnownParams.Fx);
            if (fxVer < request.Version && request.Parameters.Contains(WellKnownParams.Fx))
                return Unit.Task;
                
            var (_, E) = _storage.Get(WellKnownParams.E);
            
            _logger.LogDebug($"Calculating {nameof(Fe)}: {request?.Version}");
            var fe = _functions.Fe(E, Fcd, Fx);

            _storage.SaveResult(request.Version, fe);
            return Unit.Task;
        }
    }
}