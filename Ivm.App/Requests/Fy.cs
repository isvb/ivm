﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Ivm.Domain;
using Ivm.Storage;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Ivm.App.Requests
{
    public class Fy : IRequest
    {
        public Fy(int version, HashSet<string> parameters)
        {
            Version = version;
            Parameters = parameters;
        }
        
        public int Version { get; set; }
        
        /// <summary>
        /// Set of parameters that must have version >= <code>Version</code>
        /// </summary>
        public HashSet<string> Parameters { get; set; }
    }
    
    public class FyHandler : IRequestHandler<Fy>
    {
        private readonly IStorage _storage;
        private readonly IFunctions _functions;
        private readonly IMediator _mediator;
        private readonly ILogger<Fy> _logger;

        public FyHandler(IStorage storage, IFunctions functions, IMediator mediator, ILogger<Fy> logger)
        {
            _storage = storage;
            _functions = functions;
            _mediator = mediator;
            _logger = logger;
        }
        
        public Task<Unit> Handle(Fy request, CancellationToken cancellationToken)
        {
            _logger.LogTrace($"Executing {nameof(Fy)}: {request?.Version}");
            
            var (_, Y) = _storage.Get(WellKnownParams.Y);
            
            _logger.LogDebug($"Calculating {nameof(Fy)}: {request?.Version}");
            var fy = _functions.Fy(Y);
            _storage.Save(WellKnownParams.Fy, fy, request.Version);

            var fcd = new Fcd(request.Version, request.Parameters);
            return _mediator.Send(fcd);
        }
    }
}