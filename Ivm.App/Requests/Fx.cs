﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Ivm.Domain;
using Ivm.Storage;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Ivm.App.Requests
{
    public class Fx : IRequest
    {
        public Fx(int version, HashSet<string> parameters)
        {
            Version = version;
            Parameters = parameters;
        }
        
        public int Version { get; set; }
        
        /// <summary>
        /// Set of parameters that must have version >= <code>Version</code>
        /// </summary>
        public HashSet<string> Parameters { get; set; }
    }
    
    public class FxHandler : IRequestHandler<Fx>
    {
        private readonly IStorage _storage;
        private readonly IFunctions _functions;
        private readonly IMediator _mediator;
        private readonly ILogger<Fx> _logger;

        public FxHandler(IStorage storage, IFunctions functions, IMediator mediator, ILogger<Fx> logger)
        {
            _storage = storage;
            _functions = functions;
            _mediator = mediator;
            _logger = logger;
        }
        
        public Task<Unit> Handle(Fx request, CancellationToken cancellationToken)
        {
            _logger.LogTrace($"Executing {nameof(Fx)}: {request?.Version}");
            
            var (_, X) = _storage.Get(WellKnownParams.X);
            
            _logger.LogDebug($"Calculating {nameof(Fx)}: {request?.Version}");
            var fx = _functions.Fx(X);
            _storage.Save(WellKnownParams.Fx, fx, request.Version);

            var fe = new Fe(request.Version, request.Parameters);
            return _mediator.Send(fe);
        }
    }
}