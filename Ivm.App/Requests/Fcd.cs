﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Ivm.Domain;
using Ivm.Storage;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Ivm.App.Requests
{
    public class Fcd : IRequest
    {
        public Fcd(int version, HashSet<string> parameters)
        {
            Version = version;
            Parameters = parameters;
        }
        
        public int Version { get; set; }
        
        /// <summary>
        /// Set of parameters that must have version >= <code>Version</code>
        /// </summary>
        public HashSet<string> Parameters { get; set; }
    }
    
    public class FcdHandler : IRequestHandler<Fcd>
    {
        private readonly IStorage _storage;
        private readonly IFunctions _functions;
        private readonly IMediator _mediator;
        private readonly ILogger<FcdHandler> _logger;

        public FcdHandler(IStorage storage, IFunctions functions, IMediator mediator, ILogger<FcdHandler> logger)
        {
            _storage = storage;
            _functions = functions;
            _mediator = mediator;
            _logger = logger;
        }
        
        public Task<Unit> Handle(Fcd request, CancellationToken cancellationToken)
        {
            _logger.LogTrace($"Executing {nameof(Fcd)}: {request?.Version}");
            
            var (fabVer, Fab) = _storage.Get(WellKnownParams.Fab);
            if (fabVer < request.Version && request.Parameters.Contains(WellKnownParams.Fab))
                return Unit.Task;
            
            var (fyVer, Fy) = _storage.Get(WellKnownParams.Fy);
            if (fyVer < request.Version && request.Parameters.Contains(WellKnownParams.Fy))
                return Unit.Task;
                
            var (_, C) = _storage.Get(WellKnownParams.C);
            var (_, D) = _storage.Get(WellKnownParams.D);
            
            _logger.LogDebug($"Calculating {nameof(Fcd)}: {request?.Version}");
            var fcd = _functions.Fcd(C, D, Fab, Fy);
            _storage.Save(WellKnownParams.Fcd, fcd, request.Version);

            var fe = new Fe(request.Version, request.Parameters);
            return _mediator.Send(fe);
        }
    }
}