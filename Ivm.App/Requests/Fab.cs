﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Ivm.Domain;
using Ivm.Storage;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Ivm.App.Requests
{
    public class Fab : IRequest
    {
        public Fab(int version, HashSet<string> parameters)
        {
            Version = version;
            Parameters = parameters;
        }
        
        public int Version { get; set; }
        
        /// <summary>
        /// Set of parameters that must have version >= <code>Version</code>
        /// </summary>
        public HashSet<string> Parameters { get; set; }
    }
    
    public class FabHandler : IRequestHandler<Fab>
    {
        private readonly IStorage _storage;
        private readonly IFunctions _functions;
        private readonly IMediator _mediator;
        private readonly ILogger<FabHandler> _logger;

        public FabHandler(IStorage storage, IFunctions functions, IMediator mediator, ILogger<FabHandler> logger)
        {
            _storage = storage;
            _functions = functions;
            _mediator = mediator;
            _logger = logger;
        }
        
        public Task<Unit> Handle(Fab request, CancellationToken cancellationToken)
        {
            _logger.LogTrace($"Executing {nameof(Fab)}: {request?.Version}");
            
            var (_, A) = _storage.Get(WellKnownParams.A);
            var (_, B) = _storage.Get(WellKnownParams.B);
            _logger.LogDebug($"Calculating {nameof(Fab)}: {request?.Version}");
            var fab = _functions.Fab(A, B);
            _storage.Save(WellKnownParams.Fab, fab, request.Version);

            var fcd = new Fcd(request.Version, request.Parameters);
            return _mediator.Send(fcd);
        }
    }
}
