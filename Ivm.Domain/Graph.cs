﻿using System.Collections.Generic;

namespace Ivm.Domain
{
    // Simplified a bit, since there are no nodes with multiple outgoing vertices
    public class Graph
    {
        private Dictionary<string, string> _adjacencyList = new();

        public void AddVertex(string from, string to) => _adjacencyList[from] = to;

        public string GetAdjacentNode(string node) => _adjacencyList[node];

        public HashSet<string> GetNodesToVisit(IEnumerable<string> fromNodes)
        {
            var nodesToVisit = new HashSet<string>();
            foreach (var startNode in fromNodes)
            {
                var node = startNode;
                while (node is not null)
                {
                    if (_adjacencyList.TryGetValue(node, out node))
                    {
                        if (!nodesToVisit.Add(node))
                            break;
                    }
                }
            }

            return nodesToVisit;
        }
    }
}