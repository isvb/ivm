﻿namespace Ivm.Domain
{
    public interface IFunctions
    {
        string Fab(string a, string b);
        string Fy(string y);
        string Fx(string x);
        string Fcd(string c, string d, string fab, string fy);
        string Fe(string e, string fcd, string fx);
    }

    public class Functions : IFunctions
    {
        public string Fab(string a, string b) => $"Fab({a},{b})";

        public string Fy(string y) => $"Fy({y})";

        public string Fx(string x) => $"Fx({x})";
        
        public string Fcd(string c, string d, string fab, string fy) => $"Fcd({c},{d},{fab},{fy})";

        public string Fe(string e, string fcd, string fx) => $"Fe({e},{fcd},{fx})";
    }
}
