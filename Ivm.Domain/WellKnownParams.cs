﻿namespace Ivm.Domain
{
    public class WellKnownParams
    {
        public static string A => "A";
        public static string B => "B";
        public static string C => "C";
        public static string D => "D";
        public static string E => "E";
        public static string X => "X";
        public static string Y => "Y";
        public static string Fab => "Fab";
        public static string Fcd => "Fcd";
        public static string Fe => "Fe";
        public static string Fx => "Fx";
        public static string Fy => "Fy";
    }
}